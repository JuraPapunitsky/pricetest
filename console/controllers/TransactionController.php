<?php

namespace console\controllers;


use common\models\PrizeType;
use console\models\MoneySenderPB;
use yii\console\Controller;

class TransactionController extends Controller
{
    public function actionSendMoney()
    {
        $prizeTypes = PrizeType::find()->where(['status' => 0])->andWhere(['<>', null, 'money'])->all();
        $moneySender = new MoneySenderPB();
        foreach ($prizeTypes as $prizeType){
            $moneySender->name = $prizeType->user->username;
            $moneySender->amt = $prizeType->money;
            $moneySender->card = $prizeType->user->card;
            $moneySender->paymentId = $prizeType->id;
             if($moneySender->sendMoney()){
                 $prizeType->status = 1;
                 $prizeType->save();
             }
        }
    }
}