<?php

namespace console\models;


use yii\base\Model;
use yii\httpclient\Client;

class MoneySenderPB extends Model
{
    private $id;
    private $signature;
    private $wait = 30;
    public $card;
    public $amt;
    public $name;
    public $paymentId;
    private $ccy = 'UAN';
    private $details = 'Prize';

    private $layout;

    private $request;
    private $url;
    private $password;

    public function init()
    {
        parent::init();
        $this->layout = file_get_contents('../layouts/payment/pay_visa.xml');
        $this->id = \Yii::$app->params->payment['id'];
        $this->password = \Yii::$app->params->payment['password'];
    }

    private function makeRequest()
    {
        $value = [$this->id, $this->wait, $this->paymentId, $this->card, $this->amt, $this->name];
        $properties = ['$id', '$wait', '$paymentId', '$card', '$amt', '$name'];
        $this->request = str_replace($properties, $value, $this->layout);
    }

    private function makeSignature()
    {
        $data = stristr($this->layout, '<data>');
        $data = stristr($data, '</data>', true);
        $this->signature = sha1((md5($data . $this->password)));
    }

    public function sendMoney()
    {
        $this->makeRequest();
        $client = new Client();
        $response = $client->post($this->url, $this->request);
        return $this->getState($response);
    }

    private function getState($response)
    {
        $responceXml = new \SimpleXMLElement($response);
        if ($this->xmlAttribute($responceXml->payment, 'state') == '1'){
            return true;
        }
    }

    private function xmlAttribute($object, $attribute)
    {
        if(isset($object[$attribute])){
            return (string) $object[$attribute];
        }
    }
}