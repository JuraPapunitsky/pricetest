<?php

use yii\db\Migration;

/**
 * Class m180129_074320_transaction
 */
class m180129_074320_transaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'prize_type_id' => $this->integer(),
            'transaction_type' => $this->integer(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_074320_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_074320_transaction cannot be reverted.\n";

        return false;
    }
    */
}
