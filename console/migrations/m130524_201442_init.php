<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'cart_num' => $this->integer(16),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'role' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('point_user', [
            'id' => $this->integer(),
            'amount' => $this->double(),
            'user_id' => $this->integer(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('FK_POINT_USER', '{{%point_user}}', 'user_id');
        $this->addForeignKey('FK_POINT_USER', '{{%point_user}}', 'user_id', '{{%user}}', 'id');

        $this->createTable('money_user', [
            'id' => $this->integer(),
            'user_id' => $this->integer(),
            'amount' => $this->integer(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('FK_MONEY_USER', '{{%money_user}}', 'user_id');
        $this->addForeignKey('FK_MONEY_USER', '{{%money_user}}', 'user_id', '{{%user}}', 'id');

        $this->createTable('product', [
            'id' => $this->integer(),
            'name' => $this->string(32),
            'description' => $this->text(),
            'cost' => $this->double(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('money_system', [
            'id' => $this->integer(),
            'amount' => $this->double(),
            'price_amount' => $this->double(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);


        $this->createTable('prize_type', [
            'id' => $this->integer(),
            'money' => $this->double(),
            'points' => $this->double(),
            'product' => $this->integer(),
            'user_id' => $this->integer(),
            'status' => $this->boolean(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('FK_PRIZE_USER', '{{%prize_type}}', 'user_id');
        $this->addForeignKey('FK_PRIZE_USER', '{{%prize_type}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
