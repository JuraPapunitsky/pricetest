<?php

use yii\db\Migration;

/**
 * Class m180130_085557_trabs_userid
 */
class m180130_085557_trabs_userid extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction}}', 'user_id', $this->integer());

        $this->createIndex('FK_TRANS_USER', '{{%transaction}}', 'user_id');
        $this->addForeignKey('FK_TRANS_USER', '{{%transaction}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180130_085557_trabs_userid cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_085557_trabs_userid cannot be reverted.\n";

        return false;
    }
    */
}
