<?php

use yii\db\Migration;

/**
 * Class m180129_075723_update_tables
 */
class m180129_075723_update_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('money_system', 'id', $this->primaryKey());
        $this->alterColumn('money_user', 'id', $this->primaryKey());
        $this->alterColumn('point_user', 'id', $this->primaryKey());
        $this->alterColumn('product', 'id', $this->primaryKey());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_075723_update_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_075723_update_tables cannot be reverted.\n";

        return false;
    }
    */
}
