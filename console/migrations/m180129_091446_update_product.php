<?php

use yii\db\Migration;

/**
 * Class m180129_091446_update_product
 */
class m180129_091446_update_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'status', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_091446_update_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_091446_update_product cannot be reverted.\n";

        return false;
    }
    */
}
