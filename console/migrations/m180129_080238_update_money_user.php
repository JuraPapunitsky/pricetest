<?php

use yii\db\Migration;

/**
 * Class m180129_080238_update_money_user
 */
class m180129_080238_update_money_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('money_user', 'amount', $this->double());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_080238_update_money_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_080238_update_money_user cannot be reverted.\n";

        return false;
    }
    */
}
