<?php

use yii\db\Migration;

/**
 * Class m180129_074948_transaction_update
 */
class m180129_074948_transaction_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%prize_type}}', 'id', $this->primaryKey());
        $this->createIndex('FK_TRANSACTION_PRIZE_TYPE', '{{%transaction}}', 'prize_type_id');
        $this->addForeignKey('FK_TRANSACTION_PRIZE_TYPE', '{{%transaction}}', 'prize_type_id', '{{%prize_type}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_074948_transaction_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_074948_transaction_update cannot be reverted.\n";

        return false;
    }
    */
}
