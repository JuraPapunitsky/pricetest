<?php

/* @var $this yii\web\View */
/* @var $prizeType \common\models\PrizeType */

use yii\helpers\Html;

$this->title = 'Point Prize!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-prize">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron">
        <h1>Поздравляем вы выиграли <?= $prizeType->points ?> балов лояльности!</h1>
        <p class="lead">Балы Вам были зачислены на счет.</p>
    </div>
</div>
