<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Поздравляем!</h1>

        <p class="lead">У вас есть возможность получить приз Нажмите на кнопку "Получить приз!"</p>

        <p><?= \yii\helpers\Html::a('Получить приз!', ['site/get-prize'], ['class' => 'btn btn-lg btn-success']) ?></p>
    </div>

</div>


