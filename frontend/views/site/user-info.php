<?php

/* @var $this yii\web\View */

/* @var $user \frontend\models\User */

use yii\helpers\Html;

$this->title = 'User Info';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-user-info">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>№</td>
                    <td>Дениги</td>
                    <td>Балы</td>
                    <td>Товар</td>
                    <td>Статус</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($user->prizeTypes as $prizeType) { ?>
                    <tr>
                        <td><?= $prizeType->id ?></td>
                        <td><?= $prizeType->money ?></td>
                        <td><?= $prizeType->points ?></td>
                        <td><?php if ($prizeType->product !== null) {
                                echo \common\models\Product::findOne($prizeType->product)->name;
                            } else {
                                echo '';
                            }
                            ?></td>
                        <td><?php if ($prizeType->status == 1) {
                                echo 'Получено';
                            } else {
                                echo 'Не получено';
                            } ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <div class="col-md-4"></div>
    </div>
</div>
