<?php

/* @var $this yii\web\View */
/* @var $prizeType \common\models\PrizeType */

use yii\helpers\Html;

$this->title = 'Product Prize!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-prize">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron">
        <h1>Поздравляем вы выиграли <?= \common\models\Product::findOne($prizeType->product)->name?> !</h1>

        <p class="lead">У вас есть возможность перевести <?= \common\models\Product::findOne($prizeType->product)->name ?> в деньги. Для перевода нажмите на кнопку "Перевести в деньги"</p>

        <p><?= \yii\helpers\Html::a('Перевести в деньги', ['site/convert-to-money', 'prize_type_id' => $prizeType->id], ['class' => 'btn btn-lg btn-primary']) ?></p>
    </div>


</div>
