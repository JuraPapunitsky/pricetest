<?php

/* @var $this yii\web\View */
/* @var $prizeType \common\models\PrizeType */

use yii\helpers\Html;

$this->title = 'Money Prize!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-prize">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron">
        <h1>Поздравляем вы выиграли <?= $prizeType->money ?> денег!</h1>

        <p class="lead">У вас есть возможность перевести деньги в балы лояльности. Для перевода нажмите на кнопку "Перевести в балы"</p>

        <p><?= \yii\helpers\Html::a('Перевести в балы', ['site/convert-to-points', 'prize_type_id' => $prizeType->id], ['class' => 'btn btn-lg btn-primary']) ?></p>
    </div>


</div>
