<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "money_system".
 *
 * @property int $id
 * @property double $amount
 * @property double $price_amount
 * @property int $created_at
 * @property int $updated_at
 */
class MoneySystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money_system';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'price_amount'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Amount',
            'price_amount' => 'Price Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
