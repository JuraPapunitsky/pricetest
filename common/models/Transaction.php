<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $prize_type_id
 * @property int $transaction_type
 * @property int $created_at
 * @property int $updated_at
 * @property int $user_id
 *
 * @property PrizeType $prizeType
 *
 */
class Transaction extends \yii\db\ActiveRecord
{
    const MONEY_TO_POINTS = 0;
    const PRODUCT_TO_MONEY = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prize_type_id', 'transaction_type', 'created_at', 'updated_at'], 'integer'],
            [['prize_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrizeType::className(), 'targetAttribute' => ['prize_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prize_type_id' => 'Prize Type ID',
            'transaction_type' => 'Transaction Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizeType()
    {
        return $this->hasOne(PrizeType::className(), ['id' => 'prize_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $type
     * @param PrizeType $prizeType
     * @return bool
     */
    public function createTransaction($type, PrizeType $prizeType)
    {
        $transaction = new Transaction();
        $transaction->transaction_type = $type;
        $transaction->prize_type_id = $prizeType->id;
        $transaction->user_id = Yii::$app->user->id;
        return $transaction->save();

    }
}
