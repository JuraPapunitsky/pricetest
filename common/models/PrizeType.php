<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "prize_type".
 *
 * @property int $id
 * @property double $money
 * @property double $points
 * @property int $product
 * @property int $user_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property MoneySystem $moneySystem
 */
class PrizeType extends \yii\db\ActiveRecord
{
    const POINT_CONVERT_COEFFICIENT = 3;

    private $user;
    private $moneySystem;


    public function init()
    {
        parent::init();
        $this->moneySystem = MoneySystem::findOne(1);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prize_type';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product', 'user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['money', 'points'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'money' => 'Money',
            'points' => 'Points',
            'product' => 'Product',
            'user_id' => 'User ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    private function getRandNum()
    {
        $min = 1;
        $max = 3;
        $randomNum = null;
        $moneySystem = MoneySystem::findOne(1)->amount;
        $productCount = Product::find()->where(['status' => 1])->count();
            if ($moneySystem < 10 && $productCount < 1){
                $randomNum = 2;
            } else if ($moneySystem < 10){
                $randomNum = mt_rand(2, $max);
            } else if ($productCount < 1) {
                $randomNum = mt_rand($min, 2);
            } else {
                $randomNum = mt_rand($min, $max);
            }
            return $randomNum;

    }


    /**
     * @return string the name of view
     */
    public function getPrice()
    {
        if (($this->user_id = Yii::$app->user->id) !== null) {
            $randNum = $this->getRandNum();
            if ($randNum == 1) {
                if ($this->getMoneyPrize() == true){
                    return 'money-prize';
                }
            } else if ($randNum == 2) {
                 if ($this->getPointsPrize() == true){
                     return 'point-prize';
                 }
            } else if ($randNum == 3) {
                 if ($this->getProductPrice() == true){
                     return 'product-prize';
                 }else {
                     return '445544';
                 }
            }
        }
    }

    private function getMoneyPrize()
    {
        $this->money = mt_rand(1, 10) * 10;
        $this->status = 0;
        $this->moneySystem->price_amount = $this->moneySystem->price_amount - $this->money;
        if ($this->moneySystem->price_amount < 0){
            $this->money = $this->moneySystem->price_amount;
            $this->moneySystem->price_amount = 0;
        }
        $this->moneySystem->save();
        $moneyUser = MoneyUser::findOne(['user_id' => $this->user_id]);
        $moneyUser->amount = $moneyUser->amount + $this->money;
        $moneyUser->save();
        return $this->save();
    }

    /**
     * @return bool
     */
    private function getPointsPrize()
    {
        $this->points = mt_rand(1, 10) * 10;
        $this->status = 1;
        if ($this->save()){
            if (($pointUser = PointUser::findOne(['user_id' => $this->user_id])) !== null) {
                $pointUser->setPoints($this->points);
                return $pointUser->save();
            }else {
                $pointUser = new PointUser();
                $pointUser->user_id = $this->user_id;
                $pointUser->amount = $this->points;
                return $pointUser->save();
            }
        }
    }


    private function getProductPrice()
    {
        $product = Product::find()->where(['status' => 1])->orderBy(['rand()' => SORT_DESC])->limit(1)->one();
        $this->product = $product->id;
        $this->user_id = Yii::$app->user->id;
        $product->status = 0;
        $product->save();
        $this->status = 0;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function convertMoneyToPoints()
    {
        if ($this->product === null && $this->points === null) {
            $pointCount = self::POINT_CONVERT_COEFFICIENT * $this->money;
            if (($pointUser = PointUser::findOne(['user_id' => $this->user_id])) !== null) {
                $pointUser->setPoints($pointCount);
                if ($pointUser->save()) {
                    $this->status = 1;
                    $this->save();
                    $moneyUser = MoneyUser::findOne(['user_id' => $this->user_id]);
                    $moneyUser->amount = $moneyUser->amount - $this->money;
                    $moneyUser->save();

                    $this->moneySystem->price_amount = $this->moneySystem->price_amount + $this->money;
                    $this->moneySystem->save();
                }
            } else {
                $pointUser = new PointUser();
                $pointUser->amount = $pointUser;
                $pointUser->user_id = $this->user_id;
                if ($pointUser->save()) {
                    $this->status = 1;
                    $this->save();
                    $moneyUser = MoneyUser::findOne(['user_id' => $this->user_id]);
                    $moneyUser->amount = $moneyUser->amount - $this->money;
                    $moneyUser->save();
                    $this->moneySystem->price_amount = $this->moneySystem->price_amount + $this->money;
                    $this->moneySystem->save();
                }
            }
            $transaction = new Transaction();
            return $transaction->createTransaction(Transaction::MONEY_TO_POINTS, $this);
        }
    }

    /**
     *
     */
    public function convertToMoney()
    {
        if ($this->money === null && $this->points == null){
            if (($product = Product::findOne($this->product)) !== null){
                if (($moneyUser = MoneyUser::findOne(['user_id' => $this->user_id])) !== null){
                    $moneyUser->amount = $moneyUser->amount + $product->cost;
                    $product->status = 1;
                    $product->save();
                    if ($moneyUser->save()){
                        $this->status = 1;
                        $this->save();
                    }
                }else {
                    $moneyUser = new MoneyUser();
                    $moneyUser->user_id = $this->user_id;
                    $moneyUser->amount = $product->cost;
                    $product->status = 1;
                    $product->save();
                    if ($moneyUser->save()) {
                        $this->status = 1;
                        $this->save();
                    }
                }

                $transaction = new Transaction();
                if ($transaction->createTransaction(Transaction::PRODUCT_TO_MONEY, $this)){
                    $this->moneySystem->amount = $this->moneySystem->amount - $product->cost;
                    return $this->moneySystem->save();
            }
            }
        }
    }
}
