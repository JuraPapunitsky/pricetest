<?php
/**
 * Created by PhpStorm.
 * User: wsst17
 * Date: 30.01.18
 * Time: 10:42
 */

namespace common\tests\unit\models;


use Codeception\Test\Unit;
use common\models\PointUser;
use common\models\PrizeType;
use common\models\Transaction;
use frontend\models\User;
use PHPUnit_Framework_TestResult;

class ConversionTest extends Unit
{

    public function testConversionUser()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            $userPointsPrizes = PrizeType::find()->where(['money' => null])->andWhere(['product' => null])->all();
            $totalPrizePoints = 0;
            foreach ($userPointsPrizes as $userPointsPrize) {
                $totalPrizePoints = $totalPrizePoints + $userPointsPrize->points;
            }
            $pointUser = PointUser::findOne(['user_id' => $user->id]);
            $conversionTotal = $pointUser->amount - $totalPrizePoints;
            $transactions = Transaction::findAll(['user_id' => $user->id, 'transaction_type' => Transaction::MONEY_TO_POINTS]);
            $conversionMoney = 0;
            foreach ($transactions as $transaction) {
                $conversionMoney = $conversionMoney + $transaction->prizeType->money;
            }

            expect('Conversion is correct for user ' . $user->username, $conversionTotal / 3 == $conversionMoney)->true();
            expect('Conversion is not correct for user ' . $user->username, $conversionTotal / 3 == $conversionMoney)->false();
        }

    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public
    function count()
    {
        // TODO: Implement count() method.
    }

    public
    function run(PHPUnit_Framework_TestResult $result = null)
    {
        // TODO: Implement run() method.
    }
}